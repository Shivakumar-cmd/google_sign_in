/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import {
  Alert,
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  useColorScheme,
  View,
  Linking,
} from 'react-native';
GoogleSignin.configure({
  scopes: ['email'], // what API you want to access on behalf of the user, default is email and profile
  webClientId:
    '624400481164-r65iqpalqufn1kidfpesth0d786o5k8u.apps.googleusercontent.com',
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  // hostedDomain: '', // specifies a hosted domain restriction
  // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  // forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
  // accountName: '', // [Android] specifies an account name on the device that should be used
  // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});
const App = () => {
  const [text, setText] = useState('');
  const [user, setUser] = useState(undefined);
  const onChangeText = e => {
    setText(e);
  };
  useEffect(() => {
    getCurrentUser();

    async () => {
      try {
        await GoogleSignin.hasPlayServices({
          showPlayServicesUpdateDialog: true,
        });
        // google services are available
      } catch (err) {
        console.error('play services are not available');
      }
    };
  }, []);
  const confirm = () => {
    if (text === '') {
      Alert.alert('enter email');
      return;
    } else {
      Linking.openURL('');
    }
  };

  const _signIn = async () => {
    try {
      console.log('b', userInfo);
      await GoogleSignin.hasPlayServices();

      const userInfo = await GoogleSignin.signIn();
      console.log('a', userInfo);
      setUser({userInfo});
      console.log('b', userInfo);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('IN SIGN_IN_CANCELLED');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('IN IN_PROGRESS');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log('IN PLAY_SERVICES_NOT_AVAILABLE');
      } else {
        // some other error happened
        console.log(error);
      }
    }
  };

  const getCurrentUser = async () => {
    const currentUser = await GoogleSignin.getCurrentUser();

    setUser({currentUser});
  };
  return (
    <View style={styles.container}>
      <Text style={styles.text}>IMAP</Text>
      <View>
        <TextInput
          style={styles.input}
          onChangeText={e => onChangeText(e)}
          value={text}
          placeholder="Enter Email"
          textContentType="emailAddress"
        />

        <View style={styles.button}>
          <Button title="Submit" onPress={confirm} />
        </View>
      </View>
      <GoogleSigninButton
        style={{width: 192, height: 48}}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark}
        onPress={_signIn}
        // disabled={this.state.isSigninInProgress}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
  },
  button: {
    margin: 12,
  },
});
export default App;

// import React, {useEffect, useState} from 'react';
// import {
//   StyleSheet,
//   View,
//   StatusBar,
//   TouchableOpacity,
//   Text,
//   Image,
// } from 'react-native';
// import {
//   GoogleSignin,
//   GoogleSigninButton,
//   statusCodes,
// } from '@react-native-google-signin/google-signin';
// GoogleSignin.configure({
//   webClientId:
//     '624400481164-r65iqpalqufn1kidfpesth0d786o5k8u.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
// });
// const App = () => {
//   const [isLoggedIn, setIsLoggedIn] = useState(false);
//   const [userInfo, setUserInfo] = useState(null);
//   useEffect(() => {
//     getCurrentUserInfo();
//   }, []);
//   const getCurrentUserInfo = async () => {
//     try {
//       const userInfo = await GoogleSignin.signInSilently();
//       console.log(userInfo);
//       setIsLoggedIn(true);
//       setUserInfo(userInfo);
//     } catch (error) {
//       if (error.code === statusCodes.SIGN_IN_REQUIRED) {
//         // user has not signed in yet
//       } else {
//         // some other error
//       }
//     }
//   };

//   const _signIn = async () => {
//     try {
//       await GoogleSignin.hasPlayServices();
//       const userInfo = await GoogleSignin.signIn();
//       console.log(userInfo);
//       setIsLoggedIn(true);
//       setUserInfo(userInfo);
//     } catch (error) {
//       console.log(error);
//       if (error.code === statusCodes.SIGN_IN_CANCELLED) {
//         // user cancelled the login flow
//       } else if (error.code === statusCodes.IN_PROGRESS) {
//         // operation (e.g. sign in) is in progress already
//       } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
//         // play services not available or outdated
//       } else {
//         // some other error happened
//       }
//     }
//   };

//   const _signOut = async () => {
//     try {
//       await GoogleSignin.revokeAccess();
//       await GoogleSignin.signOut();
//       setIsLoggedIn(false);
//     } catch (error) {
//       console.error(error);
//     }
//   };
//   return (
//     <>
//       <StatusBar barStyle="dark-content" />
//       <View style={styles.container}>
//         {!isLoggedIn ? (
//           <GoogleSigninButton
//             style={{width: 192, height: 48}}
//             size={GoogleSigninButton.Size.Wide}
//             color={GoogleSigninButton.Color.Dark}
//             onPress={_signIn}
//           />
//         ) : (
//           <>
//             <Text>Email: {userInfo ? userInfo.user.email : ''}</Text>
//             <Text>Name: {userInfo ? userInfo.user.name : ''}</Text>
//             <TouchableOpacity style={styles.signOutBtn} onPress={_signOut}>
//               <Text style={styles.signOutBtnText}>Signout</Text>
//             </TouchableOpacity>
//           </>
//         )}
//       </View>
//     </>
//   );
// };

// const styles = StyleSheet.create({
//   container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
//   signOutBtn: {
//     backgroundColor: '#556688',
//     padding: 10,
//     borderRadius: 10,
//   },
//   signOutBtnText: {
//     color: 'white',
//   },
// });

// export default App;
